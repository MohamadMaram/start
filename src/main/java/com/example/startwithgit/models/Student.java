package com.example.startwithgit.models;

import com.example.startwithgit.models.audits.AuditModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "students")
@Data
public class Student extends AuditModel {

    private String firstName;
    private String lastName;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Credential credential = new Credential();

    //برای انتخاب واحد توسط دانشجو
    @JsonIgnoreProperties("student")
    @OneToMany(mappedBy = "student")
    private List<Registration> registrationList;

    //برای ارزشیابی دروس انتخاب شده توسط دانشجو
    @JsonIgnoreProperties("student")
    @OneToMany(mappedBy = "student")
    private List<RegistrationRating> registrationRatingList;
}
