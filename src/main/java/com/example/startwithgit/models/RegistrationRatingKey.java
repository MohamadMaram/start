package com.example.startwithgit.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Mohamad Zarei Maram
 * @since 2/12/2023
 */
@Data
@Embeddable
public class RegistrationRatingKey implements Serializable {

    @Column(name = "student_id")
    Long studentId;

    @Column(name = "registration_id")
    Long registrationId;
}
