package com.example.startwithgit.models.commands;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */
@Data
public class RegistrationCommand {

    //TODO ->  we need to handle validation in this class
    private long studentId;
    private List<Long> programIds = new ArrayList<>();
}
