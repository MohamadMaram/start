package com.example.startwithgit.models.commands;

import com.example.startwithgit.models.RegistrationRatingKey;
import lombok.Data;

/**
 * @author Mohamad Zarei Maram
 * @since 10/4/2023
 */
@Data
public class RegistrationRatingCommand {

    private RegistrationRatingKey registrationRatingKey;
    private int range;
}
