package com.example.startwithgit.models.commands;

import com.sun.xml.bind.v2.TODO;
import lombok.Data;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */

@Data
public class ProgramCommand {

    //TODO ->  we need to handle validation in this class
    private long professorId;
    private long courseId;
    private int termNumber;
}
