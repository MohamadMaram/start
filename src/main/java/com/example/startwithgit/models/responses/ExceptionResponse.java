package com.example.startwithgit.models.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Mohamad Zarei Maram
 * @since 10/6/2023
 */
@Data
@AllArgsConstructor
public class ExceptionResponse {

    private int status;
    private String message ;

}
