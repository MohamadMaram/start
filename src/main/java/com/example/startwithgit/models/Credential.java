package com.example.startwithgit.models;

import com.example.startwithgit.models.audits.AuditModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Mohamad Zarei Maram
 * @since 2/3/2023
 */
@Entity
@Table(name = "credentials")
@Data
public class Credential extends AuditModel {

    @NotBlank
    @NotNull
    @Column(nullable = false , unique = true)
    private String username;

    @NotBlank
    @NotNull
    @Column(nullable = false)
    private String password;

    private String role;

}
