package com.example.startwithgit.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

/**
 * @author Mohamad Zarei Maram
 * @since 2/12/2023
 */
@Data
@Entity(name = "registrations_rating")
public class RegistrationRating {

    // TODO: 2/12/2023 we need to change AuditModel to handle this type of class that dont extend AuditModel
    @EmbeddedId
    private RegistrationRatingKey registrationRatingKey;

    @ManyToOne
    @JsonIgnoreProperties({"registrationList" , "registrationRatingList"})
    @MapsId("studentId")
    @JoinColumn(name = "student_id")
    private Student student;

    @OneToOne
    @JsonIgnoreProperties({"student" , "program" , "registrationRating"})
    @MapsId("registrationId")
    @JoinColumn(name = "registration_id")
    private Registration registration;

    // TODO: 2/12/2023 int type can be changes to Enum
    private int range;
}
