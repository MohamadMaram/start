package com.example.startwithgit.models;

import com.example.startwithgit.models.audits.AuditModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

/**
 * @author Mohamad Zarei Maram
 * @since 2/11/2023
 */
@Data
@Entity(name = "professors")
public class Professor extends AuditModel {

    private String firstName;
    private String lastName;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Credential credential = new Credential();

    //برای ارائه دادن درس توسط استاد
    @JsonIgnoreProperties("professor")
    @OneToMany(mappedBy = "professor")
    private List<Program> programList;
}
