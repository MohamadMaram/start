package com.example.startwithgit.models.audits;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Mohamad Zarei Maram
 * @since 1/27/2023
 */

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public abstract class AuditModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(hidden = true)
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false , updatable = false)
    @CreatedDate
    @ApiModelProperty(hidden = true)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    @LastModifiedDate
    @ApiModelProperty(hidden = true)
    private Date updated;

    @Column(name = "verified", nullable = false)
    @ApiModelProperty(hidden = true)
    private boolean verified = true;
}
