package com.example.startwithgit.models;

import com.example.startwithgit.models.audits.AuditModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

/**
 * @author Mohamad Zarei Maram
 * @since 2/11/2023
 */
@Data
@Entity(name = "registrations")
public class Registration extends AuditModel {

    private double number = 0;

    //برای انتخاب واحد توسط دانشجو
    @JsonIgnoreProperties({"registrationList" , "registrationRatingList"})
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "registration_id")
    private Program program;

    //برای ارزشیابی دروس ارائه شده توسط دانشجو
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private RegistrationRating registrationRating;

}
