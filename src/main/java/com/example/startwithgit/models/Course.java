package com.example.startwithgit.models;

import com.example.startwithgit.models.audits.AuditModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * @author Mohamad Zarei Maram
 * @since 2/5/2023
 */
@Entity
@Table(name = "courses")
//@NoArgsConstructor
@Data
public class Course extends AuditModel {

    private String name;
    private int numberOfUnit;

    /*@OneToMany(mappedBy = "course")
    private List<Registration> registrationList;*/

    //برای ارائه دادن درس توسط استاد
    @JsonIgnoreProperties("course")
    @OneToMany(mappedBy = "course")
    private List<Program> programList;
}
