package com.example.startwithgit.models;

import com.example.startwithgit.models.audits.AuditModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @author Mohamad Zarei Maram
 * @since 2/11/2023
 */

@Data
@Entity(name = "programs")
public class Program extends AuditModel {

    private int termNumber;

    //برای ارائه دادن درس توسط استاد
    @JsonIgnoreProperties({"programList"})
    @ManyToOne
    @JoinColumn(name = "professor_id")
    private Professor professor;

    @JsonIgnoreProperties({"programList"})
    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;


    //برای انتخاب واحد توسط دانشجو
    @JsonIgnoreProperties({"program"})
    @OneToMany(mappedBy = "program")
    private List<Registration> registrationList;

    /*@OneToMany(mappedBy = "program")
    private List<RegistrationRating> registrationRatingList;*/

}
