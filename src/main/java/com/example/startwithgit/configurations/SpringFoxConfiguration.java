package com.example.startwithgit.configurations;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * @author Mohamad Zarei Maram
 * @since 1/31/2023
 */
@Configuration
@EnableSwagger2
public class SpringFoxConfiguration {

    @Bean
    public Docket apiDocket(TypeResolver typeResolver) {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                //.apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.example.start"))
                .build()
                .apiInfo(apiInfo());
                /*.additionalModels(
                        typeResolver.resolve(User.class)
                );*/
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "START",
                "This is a project that i collect some features as a CV",
                "1.0",
                "",
                new Contact("Mohamad Zarei Maram", "", "mohamadmaram20@gmail.com"),
                "Apache 2.0",
                "www.apache.org/licenses/LICENSE-2.0",
                Collections.emptyList()

        );
    }
}
