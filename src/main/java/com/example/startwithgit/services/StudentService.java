package com.example.startwithgit.services;

import com.example.startwithgit.models.Student;

/**
 * @author Mohamad Zarei Maram
 * @since 1/30/2023
 */

public interface StudentService {

    Iterable<Student> findAll();
    Student save(Student student);

    Student getStudent(long id);
}
