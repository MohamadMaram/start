package com.example.startwithgit.services;

import com.example.startwithgit.models.Program;
import com.example.startwithgit.models.Student;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */

public interface ProgramService {

    Iterable<Program> getAll();
    Program save(Program program);

    Program getProgram(long id);
}
