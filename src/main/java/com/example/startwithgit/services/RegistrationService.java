package com.example.startwithgit.services;

import com.example.startwithgit.models.Program;
import com.example.startwithgit.models.Registration;

import java.util.List;

/**
 * @author Mohamad Zarei Maram
 * @since 10/4/2023
 */

public interface RegistrationService {

    Iterable<Registration> getAll();

    Registration getRegistration(long id);

    Registration save(Registration registration);

    Iterable<Registration> saveAll(Iterable<Registration> registrationList);
}
