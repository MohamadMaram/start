package com.example.startwithgit.services;

import com.example.startwithgit.models.Professor;
import com.example.startwithgit.models.Student;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */

public interface ProfessorService {

    Iterable<Professor> getAll();
    Professor save(Professor professor);

    Professor getProfessor(long id);
}
