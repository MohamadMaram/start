package com.example.startwithgit.services;

import com.example.startwithgit.models.Professor;
import com.example.startwithgit.repositories.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */

@Service
public class ProfessorServiceImpl implements ProfessorService {

    private final ProfessorRepository professorRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public ProfessorServiceImpl(ProfessorRepository professorRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.professorRepository = professorRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public Iterable<Professor> getAll() {
        return professorRepository.findAll();
    }

    @Override
    public Professor save(Professor professor) {
        professor.getCredential().setPassword(bCryptPasswordEncoder.encode(professor.getCredential().getPassword()));
        return professorRepository.save(professor);
    }

    @Override
    public Professor getProfessor(long id) {
        return professorRepository.findById(id).get();
    }
}
