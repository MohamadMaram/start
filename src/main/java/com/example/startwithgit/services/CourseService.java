package com.example.startwithgit.services;

import com.example.startwithgit.models.Course;
import com.example.startwithgit.models.Student;

/**
 * @author Mohamad Zarei Maram
 * @since 10/2/2023
 */

public interface CourseService {

    Iterable<Course> getAll();
    Course save(Course course);
    Course getCourse(long id);
}
