package com.example.startwithgit.services;

import com.example.startwithgit.models.Program;
import com.example.startwithgit.models.RegistrationRating;
import com.example.startwithgit.models.RegistrationRatingKey;

/**
 * @author Mohamad Zarei Maram
 * @since 10/4/2023
 */

public interface RegistrationRatingService {

    Iterable<RegistrationRating> getAll();

    RegistrationRating save(RegistrationRating registrationRating);

    RegistrationRating getRegistrationRating(RegistrationRatingKey id);
}
