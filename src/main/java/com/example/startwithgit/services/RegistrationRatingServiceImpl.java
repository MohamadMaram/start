package com.example.startwithgit.services;

import com.example.startwithgit.models.RegistrationRating;
import com.example.startwithgit.models.RegistrationRatingKey;
import com.example.startwithgit.repositories.RegistrationRatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

/**
 * @author Mohamad Zarei Maram
 * @since 10/4/2023
 */
@Service
public class RegistrationRatingServiceImpl implements RegistrationRatingService {

    private final RegistrationRatingRepository registrationRatingRepository;

    @Autowired
    public RegistrationRatingServiceImpl(RegistrationRatingRepository registrationRatingRepository) {
        this.registrationRatingRepository = registrationRatingRepository;
    }

    @Override
    public Iterable<RegistrationRating> getAll() {
        return registrationRatingRepository.findAll();
    }

    @Override
    public RegistrationRating save(RegistrationRating registrationRating) {

        if (registrationRatingRepository.existsById(registrationRating.getRegistrationRatingKey())) {
            throw new DuplicateKeyException("duplicate record");
        }
        return registrationRatingRepository.save(registrationRating);
    }

    @Override
    public RegistrationRating getRegistrationRating(RegistrationRatingKey id) {
        return registrationRatingRepository.findById(id).get();
    }
}
