package com.example.startwithgit.services;

import com.example.startwithgit.models.Registration;
import com.example.startwithgit.repositories.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mohamad Zarei Maram
 * @since 10/4/2023
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {

    private final RegistrationRepository registrationRepository;

    @Autowired
    public RegistrationServiceImpl(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    @Override
    public Iterable<Registration> getAll() {
        return registrationRepository.findAll();
    }

    @Override
    public Registration getRegistration(long id) {
        return registrationRepository.findById(id).get();
    }

    @Override
    public Registration save(Registration registration) {
        return registrationRepository.save(registration);
    }

    @Override
    public Iterable<Registration> saveAll(Iterable<Registration> registrationList) {
        return registrationRepository.saveAll(registrationList);
    }
}
