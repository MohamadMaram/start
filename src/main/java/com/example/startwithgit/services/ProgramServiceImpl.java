package com.example.startwithgit.services;

import com.example.startwithgit.models.Program;
import com.example.startwithgit.repositories.ProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */

@Service
public class ProgramServiceImpl implements ProgramService {

    private final ProgramRepository programRepository;

    @Autowired
    public ProgramServiceImpl(ProgramRepository programRepository) {
        this.programRepository = programRepository;
    }

    @Override
    public Iterable<Program> getAll() {
        return programRepository.findAll();
    }

    @Override
    public Program save(Program program) {
        return programRepository.save(program);
    }

    @Override
    public Program getProgram(long id) {
        return programRepository.findById(id).get();
    }
}
