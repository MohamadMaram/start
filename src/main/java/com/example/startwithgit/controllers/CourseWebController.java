package com.example.startwithgit.controllers;

import com.example.startwithgit.models.Course;
import com.example.startwithgit.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.example.startwithgit.GlobalVariables.WEB_ROUTE;

/**
 * @author Mohamad Zarei Maram
 * @since 10/2/2023
 */
@RestController
@RequestMapping(WEB_ROUTE + "/courses")
public class CourseWebController {

    private final CourseService courseService;

    @Autowired
    public CourseWebController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getCourse(@PathVariable long id ){
        return ResponseEntity.ok(courseService.getCourse(id));
    }

    @GetMapping("/allCourses")
    public ResponseEntity<?> getAllCourses(){
        return ResponseEntity.ok(courseService.getAll());
    }

    @PostMapping("/register")
    public ResponseEntity<?> courseRegister(@RequestBody Course course){
        return ResponseEntity.ok(courseService.save(course));
    }
}
