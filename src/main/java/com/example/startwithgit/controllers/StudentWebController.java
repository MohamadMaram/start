package com.example.startwithgit.controllers;

import com.example.startwithgit.components.convertors.RegistrationCommandToRegistration;
import com.example.startwithgit.components.convertors.RegistrationRatingCommandToRegistrationRating;
import com.example.startwithgit.models.Registration;
import com.example.startwithgit.models.RegistrationRating;
import com.example.startwithgit.models.Student;
import com.example.startwithgit.models.commands.RegistrationCommand;
import com.example.startwithgit.models.commands.RegistrationRatingCommand;
import com.example.startwithgit.services.RegistrationRatingService;
import com.example.startwithgit.services.RegistrationService;
import com.example.startwithgit.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.example.startwithgit.GlobalVariables.WEB_ROUTE;

/**
 * @author Mohamad Zarei Maram
 * @since 1/27/2023
 */

@RestController
@RequestMapping(WEB_ROUTE + "/students")
public class StudentWebController {

    private final StudentService studentService;
    private final RegistrationCommandToRegistration registrationCommandToRegistration;
    private final RegistrationService registrationService;
    private final RegistrationRatingCommandToRegistrationRating registrationRatingCommandToRegistrationRating;
    private final RegistrationRatingService registrationRatingService;

    @Autowired
    public StudentWebController(StudentService studentService,
                                RegistrationCommandToRegistration registrationCommandToRegistration,
                                RegistrationService registrationService, RegistrationRatingCommandToRegistrationRating registrationRatingCommandToRegistrationRating, RegistrationRatingService registrationRatingService) {
        this.studentService = studentService;
        this.registrationCommandToRegistration = registrationCommandToRegistration;
        this.registrationService = registrationService;
        this.registrationRatingCommandToRegistrationRating = registrationRatingCommandToRegistrationRating;
        this.registrationRatingService = registrationRatingService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getStudent(@PathVariable long id) {
        return ResponseEntity.ok(studentService.getStudent(id));
    }

    @GetMapping("/allStudents")
    public ResponseEntity<?> getAllStudents() {
        return ResponseEntity.ok(studentService.findAll());
    }

    @PostMapping("/register")
    public ResponseEntity<?> studentRegister(@RequestBody Student student) {
        return ResponseEntity.ok(studentService.save(student));
    }

    @PostMapping("/courseRegistration")
    public ResponseEntity<?> courseRegistration(@RequestBody RegistrationCommand registrationCommand) {
        Iterable<Registration> detachedRegistrationList = registrationCommandToRegistration.convert(registrationCommand);
        return ResponseEntity.ok(registrationService.saveAll(detachedRegistrationList));
    }

    @PostMapping("/RegistrationRating")
    public ResponseEntity<?> registrationRating(@RequestBody RegistrationRatingCommand registrationRatingCommand){
        RegistrationRating detachedRegistrationRating = registrationRatingCommandToRegistrationRating.convert(registrationRatingCommand);
        return ResponseEntity.ok(registrationRatingService.save(detachedRegistrationRating));
    }

}
