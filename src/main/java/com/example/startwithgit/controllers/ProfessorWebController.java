package com.example.startwithgit.controllers;

import com.example.startwithgit.components.convertors.ProgramCommandToProgram;
import com.example.startwithgit.models.Professor;
import com.example.startwithgit.models.Program;
import com.example.startwithgit.models.commands.ProgramCommand;
import com.example.startwithgit.services.ProfessorService;
import com.example.startwithgit.services.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.example.startwithgit.GlobalVariables.WEB_ROUTE;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */

@RestController
@RequestMapping(WEB_ROUTE + "/professors")
public class ProfessorWebController {
    private final ProfessorService professorService;
    private final ProgramCommandToProgram programCommandToProgram;
    private final ProgramService programService;

    @Autowired
    public ProfessorWebController(ProfessorService professorService, ProgramCommandToProgram programCommandToProgram, ProgramService programService) {
        this.professorService = professorService;
        this.programCommandToProgram = programCommandToProgram;
        this.programService = programService;
    }

    @GetMapping("/allProfessors")
    public ResponseEntity<?> getAllProfessors() {
        return ResponseEntity.ok(professorService.getAll());
    }

    @PostMapping("/register")
    public ResponseEntity<?> professorRegister(@RequestBody Professor professor) {
        return ResponseEntity.ok(professorService.save(professor));
    }

    @PostMapping("/makingProgram")
    public ResponseEntity<?> makingProgram(@RequestBody ProgramCommand programCommand) {
        Program detachedProgram = programCommandToProgram.convert(programCommand);
        return ResponseEntity.ok(programService.save(detachedProgram));
    }


}
