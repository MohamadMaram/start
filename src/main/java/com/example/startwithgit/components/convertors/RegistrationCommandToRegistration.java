package com.example.startwithgit.components.convertors;

import com.example.startwithgit.models.Program;
import com.example.startwithgit.models.Registration;
import com.example.startwithgit.models.Student;
import com.example.startwithgit.models.commands.RegistrationCommand;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.awt.geom.RectangularShape;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */
@Component
public class RegistrationCommandToRegistration implements Converter<RegistrationCommand, List<Registration>> {

    @Synchronized
    @Nullable
    @Override
    public List<Registration> convert(RegistrationCommand registrationCommand) {

        //TODO -> we have to fetch student and programs from DB by Id and set to registration

        List<Registration> registrationList = new ArrayList<>();
        Student student = new Student();
        student.setId(registrationCommand.getStudentId());

        registrationCommand.getProgramIds().forEach(programId -> {
            Registration registration = new Registration();
            Program program = new Program();
            program.setId(programId);

            registration.setStudent(student);
            registration.setProgram(program);
            registrationList.add(registration);
        });
        return registrationList;
    }
}
