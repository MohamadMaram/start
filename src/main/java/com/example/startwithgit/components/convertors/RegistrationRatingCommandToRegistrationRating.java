package com.example.startwithgit.components.convertors;

import com.example.startwithgit.models.Registration;
import com.example.startwithgit.models.RegistrationRating;
import com.example.startwithgit.models.Student;
import com.example.startwithgit.models.commands.RegistrationRatingCommand;
import com.example.startwithgit.services.RegistrationService;
import com.example.startwithgit.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author Mohamad Zarei Maram
 * @since 10/4/2023
 */
@Component
public class RegistrationRatingCommandToRegistrationRating implements Converter<RegistrationRatingCommand, RegistrationRating> {

    private final StudentService studentService;
    private final RegistrationService registrationService;

    @Autowired
    public RegistrationRatingCommandToRegistrationRating(StudentService studentService, RegistrationService registrationService) {
        this.studentService = studentService;
        this.registrationService = registrationService;
    }

    @Override
    public RegistrationRating convert(RegistrationRatingCommand registrationRatingCommand) {

        RegistrationRating registrationRating = new RegistrationRating();
        Student student = studentService.getStudent(registrationRatingCommand.getRegistrationRatingKey().getStudentId());
        Registration registration = registrationService.getRegistration(registrationRatingCommand.getRegistrationRatingKey().getRegistrationId());

        registrationRating.setRegistrationRatingKey(registrationRatingCommand.getRegistrationRatingKey());

        registrationRating.setStudent(student);
        registrationRating.setRegistration(registration);
        registrationRating.setRange(registrationRatingCommand.getRange());

        return registrationRating;
    }
}
