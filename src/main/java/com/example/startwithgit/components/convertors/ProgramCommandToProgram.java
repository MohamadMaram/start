package com.example.startwithgit.components.convertors;

import com.example.startwithgit.models.Course;
import com.example.startwithgit.models.Professor;
import com.example.startwithgit.models.Program;
import com.example.startwithgit.models.commands.ProgramCommand;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 * @author Mohamad Zarei Maram
 * @since 10/3/2023
 */

@Component
public class ProgramCommandToProgram implements Converter<ProgramCommand , Program> {

    @Synchronized
    @Nullable
    @Override
    public Program convert(ProgramCommand programCommand) {

        //TODO -> we have to fetch professor and course from DB by Id and set to program
        Program program = new Program();
        Professor professor = new Professor();
        Course course = new Course();

        professor.setId(programCommand.getProfessorId());
        course.setId(programCommand.getCourseId());

        program.setProfessor(professor);
        program.setCourse(course);
        program.setTermNumber(programCommand.getTermNumber());

        return program;
    }
}
