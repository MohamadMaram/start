package com.example.startwithgit.repositories;

import com.example.startwithgit.models.Professor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Mohamad Zarei Maram
 * @since 2/13/2023
 */

@Repository
public interface ProfessorRepository extends CrudRepository<Professor, Long> {
}