package com.example.startwithgit.repositories;

import com.example.startwithgit.models.RegistrationRating;
import com.example.startwithgit.models.RegistrationRatingKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Mohamad Zarei Maram
 * @since 2/13/2023
 */

@Repository
public interface RegistrationRatingRepository extends CrudRepository<RegistrationRating, RegistrationRatingKey> {
}