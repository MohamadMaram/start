package com.example.startwithgit.repositories;

import com.example.startwithgit.models.Credential;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Mohamad Zarei Maram
 * @since 2/13/2023
 */

@Repository
public interface CredentialRepository extends CrudRepository<Credential, Long> {
}